export const DELETE_USER = "user/DELETE_USER";
export const EDIT_USER = "user/EDIT_USER";
export const SUBMIT_USER = "user/SUBMIT_USER";
export const GET_KEYWORD = "user/GET_KEYWORD";

export const ADD_TASK = "task/ADD_TASK";
export const DELETE_TASK = "task/DELETE_TASK";
export const UPDATE_TASK = "task/UPDATE_TASK";
