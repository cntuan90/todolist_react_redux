import * as ActionType from "../constant";

let initialState = {
  tasks: [],
};

const taskReducer = (state = initialState, action) => {
  console.log(action);
  switch (action.type) {
    case ActionType.DELETE_TASK: {
      let taskList = [...state.tasks];
      const index = state.tasks.findIndex(
        (task) => task.id === action.payload.id
      );

      if (index !== -1) {
        taskList.splice(index, 1);
        state.tasks = taskList;
      }

      return { ...state };
    }

    case ActionType.ADD_TASK: {
      let taskList = [...state.tasks];

      const newTask = { ...action.payload, id: Math.random() };
      taskList = [...state.tasks, newTask];
      state.tasks = taskList;
      return { ...state };
    }

    case ActionType.UPDATE_TASK: {
      //UPDATE Task
      let taskList = [...state.tasks];

      const index = taskList.findIndex((task) => task.id === action.payload.id);
      if (index !== -1) {
        taskList[index].status =
          action.payload.status === "todo" ? "completed" : "todo";
      }
      state.tasks = taskList;
      return { ...state };
    }

    default:
      return { ...state };
  }
};

export default taskReducer;
