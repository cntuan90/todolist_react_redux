import { combineReducers } from "redux";
import userReducer from "./user";
import taskReducer from "./task";

const rootReducer = combineReducers({
  //key: value
  userReducer, //   userReducer: userReducer,
  taskReducer,
});

export default rootReducer;
