import React, { Component } from "react";
import ItemTask from "./item-task";
import { connect } from "react-redux";

class ListTask extends Component {
  renderTodo = () => {
    const { tasks } = this.props;
    return tasks.map((item) => {
      if (item.status === "todo") {
        return <ItemTask key={item.id} task={item} />;
      }
    });
  };

  renderCompleted = () => {
    const { tasks } = this.props;
    return tasks.map((item) => {
      if (item.status === "completed") {
        return <ItemTask key={item.id} task={item} />;
      }
    });
  };

  render() {
    return (
      <>
        <ul className="todo" id="todo">
          {this.renderTodo()}
        </ul>
        <ul className="todo" id="completed">
          {this.renderCompleted()}
        </ul>
      </>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    tasks: state.taskReducer.tasks,
  };
};

export default connect(mapStateToProps, null)(ListTask);
