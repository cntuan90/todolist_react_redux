import React, { Component } from "react";
import { connect } from "react-redux";
import { actAddTask } from "../redux/action";

class AddTask extends Component {
  constructor(props) {
    super(props);
    this.state = {
      id: "",
      content: "",
      status: "todo",
    };
  }

  handleOnChange = (event) => {
    const { name, value } = event.target;
    this.setState({
      [name]: value,
    });
  };

  handleAddTask = () => {
    this.props.addTask(this.state);
  };

  render() {
    return (
      <>
        <input
          onChange={this.handleOnChange}
          id="newTask"
          type="text"
          placeholder="Enter an activity..."
          name="content"
          value={this.state.content}
        />
        <button onClick={this.handleAddTask} id="addItem">
          <i className="fa fa-plus" />
        </button>
      </>
    );
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    addTask: (task) => {
      dispatch(actAddTask(task));
    },
  };
};

export default connect(null, mapDispatchToProps)(AddTask);
