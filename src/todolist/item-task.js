import React, { Component } from "react";
import { connect } from "react-redux";
import { actDeleteTask, actUpdateTask } from "../redux/action";

class ItemTask extends Component {
  render() {
    const { task } = this.props;
    return (
      <li>
        <span className="liName">{task.content}</span>
        <span>
          <i
            onClick={() => {
              this.props.deleteTask(task);
            }}
            className="fas fa-trash-alt"
          />
          <i
            onClick={() => {
              this.props.updateTask(task);
            }}
            className="far fa-check-circle"
          />
        </span>
      </li>
    );
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    deleteTask: (task) => {
      dispatch(actDeleteTask(task));
    },
    updateTask: (task) => {
      dispatch(actUpdateTask(task));
    },
  };
};

export default connect(null, mapDispatchToProps)(ItemTask);
