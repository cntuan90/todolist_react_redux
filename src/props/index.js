import React, { Component } from "react";
import Child from "./child";
import ChildFunction from "./childFunction";
import DemoChildren from "./children";

export default class Props extends Component {
  constructor(props) {
    super(props);
    this.state = {
      username: "Nguyen",
      lop: "BC03",
    };
  }

  handleChangeUsername = () => {
    this.setState({
      username: "Cybersoft",
    });
  };

  handleGetResetUsername = (resetUsername) => {
    this.setState({
      username: resetUsername,
    });
  };

  render() {
    return (
      <div>
        <h3>*Props</h3>
        <p>
          Username: {this.state.username} - Lop: {this.state.lop}
        </p>
        <button className="btn btn-success" onClick={this.handleChangeUsername}>
          Change username
        </button>

        <Child
          username={this.state.username}
          lop={this.state.lop}
          getResetUsername={this.handleGetResetUsername}
        />
        <ChildFunction username={this.state.username} lop={this.state.lop} />
        <DemoChildren>
          <div>
            <p>Demo Children</p>
            <p>Lorem</p>
          </div>
        </DemoChildren>
      </div>
    );
  }
}
