import React, { Component } from "react";

export default class FormValidation extends Component {
  constructor(props) {
    super(props);
    this.state = {
      values: {
        manv: "",
        tennv: "",
        email: "",
      },
      errors: {
        manv: "",
        tennv: "",
        email: "",
      },
      formValid: false,
      maValid: false,
      tenValid: false,
      emailValid: false,
    };
  }

  handleOnchange = (e) => {
    const { name, value } = e.target;
    this.setState({
      values: { ...this.state.values, [name]: value },
    });
  };

  handleErrors = (e) => {
    const { name, value } = e.target;
    let mess = value === "" ? name + " k dc rong" : "";
    let { maValid, tenValid, emailValid } = this.state;
    console.log(name, value, mess);
    switch (name) {
      case "manv":
        maValid = mess !== "" ? false : true;
        if (value && value.trim().length < 4) {
          mess = "Vui lòng nhập độ dài ký tự > 3";
          maValid = false;
        }
        break;
      case "tennv":
        tenValid = mess !== "" ? false : true;
        break;
      case "email":
        emailValid = mess !== "" ? false : true;
        if (value && !value.match(/^([\w.%+-]+)@([\w-]+\.)+([\w]{2,})$/i)) {
          mess = "Email k dung dinh dang";
          emailValid = false;
        }
        break;

      default:
        break;
    }
    this.setState(
      {
        errors: { ...this.state.errors, [name]: mess },
        maValid,
        tenValid,
        emailValid,
      },
      () => {
        console.log(this.state);
        this.formValidation();
      }
    );
  };

  formValidation = () => {
    let { maValid, tenValid, emailValid } = this.state;
    this.setState({
      formValid: maValid && tenValid && emailValid,
    });
  };

  render() {
    return (
      <div className="container d-block">
        <h3 className="title">*FormValidation</h3>
        <form>
          <div className="form-group">
            <label>Mã NV</label>
            <input
              type="text"
              className="form-control"
              name="manv"
              onChange={this.handleOnchange}
              onBlur={this.handleErrors}
              onKeyUp={this.handleErrors}
            />
            {this.state.errors.manv !== "" ? (
              <div className="alert alert-danger">{this.state.errors.manv}</div>
            ) : (
              ""
            )}
          </div>
          <div className="form-group">
            <label>Tên NV</label>
            <input
              type="text"
              className="form-control"
              name="tennv"
              onChange={this.handleOnchange}
              onBlur={this.handleErrors}
              onKeyUp={this.handleErrors}
            />
            {this.state.errors.tennv !== "" ? (
              <div className="alert alert-danger">
                {this.state.errors.tennv}
              </div>
            ) : (
              ""
            )}
          </div>
          <div className="form-group">
            <label>Email</label>
            <input
              type="email"
              className="form-control"
              name="email"
              onChange={this.handleOnchange}
              onBlur={this.handleErrors}
              onKeyUp={this.handleErrors}
            />
            {this.state.errors.email !== "" ? (
              <div className="alert alert-danger">
                {this.state.errors.email}
              </div>
            ) : (
              ""
            )}
          </div>
          <button
            type="submit"
            className="btn btn-success"
            disabled={!this.state.formValid}
          >
            Submit
          </button>
        </form>
      </div>
    );
  }
}
