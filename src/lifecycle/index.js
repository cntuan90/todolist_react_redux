import React, { Component } from "react";

export default class LifeCycle extends Component {
  constructor(props) {
    super(props);
    this.state = {
      number: 0,
    };
    console.log("constructor - chạy 1 lần duy nhất");
  }

  UNSAFE_componentWillMount() {
    console.log("componentWillMount - chạy 1 lần duy nhất");
  }

  componentDidMount() {
    //Gọi api - dom
    console.log("componentDidMount - chạy 1 lần duy nhất - chạy sau render");
  }

  UNSAFE_componentWillUpdate() {
    console.log("componentWillUpdate");
  }

  componentDidUpdate() {
    console.log("componentDidUpdate");
  }

  shouldComponentUpdate(nextProps, nextState) {
    console.log("shouldComponentUpdate", nextProps, nextState);
    if (nextState && nextState.number === 2) {
      return false;
    }
    return true;
  }

  render() {
    console.log("render");
    return (
      <div>
        <h3>*LifeCycle</h3>
        <p>Number: {this.state.number}</p>
        <button
          className="btn btn-success"
          onClick={() => {
            this.setState({
              number: this.state.number + 1,
            });
          }}
        >
          Tăng số
        </button>
      </div>
    );
  }
}
